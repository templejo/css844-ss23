widiv_SNP
	contains .vcf.gz files, one per chromosome
		these were filtered for a minor allele freq >0.02% and LD <0.99
	contains .sh scripts from Ally for renaming and filtering original data
	contains SNP_NamingConventions folder
		txt and xlsx files in this folder were used to match up incongruous variety names

widiv_phenos_2021
	contains one .csv file with all manually measured traits in EL2021
		each row represents one plot
		traits ending in P1 and P2 were measured on 2 plants per plot

widiv_phenos_2020
	contains one .csv file with all manually measured traits in EL2020
		each row represents one plot
		traits ending in P1 and P2 were measured on 2 plants per plot

widiv_phenolic_compounds
	contains one .csv file with all kernel phenolic compounds measured
		each row represents one variety
		raw traits have already been processed to account for reps etc

widiv_drone_2020and2021
	contains two folders, each with one .csv file
		DroneData_2020_Cleaned_03062023.csv
			columns are labeled with data type and date
			each row is from one plot
		2021DroneData_allCleaned_03072023.csv
			columns are labeled with the data and data type, 
				followed by a number (0 to 5) that refer to:
				sum, mean, median, stdev, min, max

reference_slides
	whiteboard.3.21.pdf
		our notes from class discussion for this module
	SidneySitar_ResearchTalk.pptx
		my MS student's recent talk about tar spot research
	2023-2-28 Genetic Mapping and GWAS.pptx
		the slides from Addie's lecture day on genotype-to-phenotype approaches

Pairs_of_DataTypes_DataSets
	files processed by Ally to combine different data types
		these are just processed versions of the other files
		there is a README in this folder as well

Mural_et_al_2022
	published datasets from OTHER research groups (not MSU) using the same
		diversity panel, studying various phenotypes
		we did NOT collect this data - but you MAY choose to use some of this
		if you want to ask questions about other traits that may influence
		some trait we did measure (e.g. tar spot) - because these are all inbred
		lines, you can still look at things like shared genetic control and/or
		phenotypic or genetic correlations across different datasets1

tar spot supplemental.xlsx
	additional file of tar spot observations across different years and locations
	these are reported on a per variety basis, not per plot
	these were grown in 2019-2020 on OFF-CAMPUS locations

merged_gene_tpms_2021MSU.csv
	RNAseq processed file from EL2021 field season on MSU campus
	one timepoint, just after flowering
	tar spot would have been present in the field

mean_seedling_phenolics.csv
	phenolic compound content in seedling tissue of plants grown in a growth chamber
	this is a subset of the full population
	these are NOT the same plants as any of the other datasets
	but they are the same inbred lines
	so, this is like the Mural data, except this is unpublished (Grotewold lab)

EL2021.spatial.csv
	plot numbers and row/range/rep data for East Lansing 2021
		Pass = row, and Range = column

EL2020.spatial.csv
	plot numbers and row/range/rep data for East Lansing 2020

Decatur2020_plotwise_tarspot.csv
	plot numbers and tar spot ratings for Decatur 2020
	includes row/range/rep/rater/etc information
	includes spatially-adjusted ratings (they don't change a lot)