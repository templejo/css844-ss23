# CSS844 SS23 Notebooks

This repository contains the code for the CSS844 course at MSU for the Spring Semester 2023.

## Project Information

- Author: Joshua A. Temple
- Email: templejo@msu.edu
- Course: CSS 844 - Frontiers in Computational Plant Science
- Semester: Spring 2023

## Notes

- Module 1 only contains the notebook and no data. The data is too large to store on GitLab.
